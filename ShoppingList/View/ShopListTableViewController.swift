//
//  ShopListTableViewController.swift
//  ShoppingList
//
//  Created by Hariharan S on 12/05/24.
//

import UIKit

class ShopListTableViewController: UITableViewController {
    
    // MARK: - Property
    
    private var shopList: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavBar()
    }
}
// MARK: - UITableViewDataSource Conformance

extension ShopListTableViewController {
    override func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        self.shopList.count
    }
    
    override func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let shopListCell = tableView.dequeueReusableCell(
            withIdentifier: "ShopListCell",
            for: indexPath
        )
        shopListCell.textLabel?.text = self.shopList[indexPath.row]
        return shopListCell
    }
}
// MARK: - Private Methods

private extension ShopListTableViewController {
    func configureNavBar() {
        self.title = "Shopping List"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(self.showInputAlert)
        )
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .refresh,
            target: self,
            action: #selector(self.resetShopListData)
        )
        self.navigationItem.leftBarButtonItem?.isEnabled = false
        
        let shareButton = UIBarButtonItem(
            barButtonSystemItem: .action,
            target: self,
            action: #selector(self.shareItems)
        )
        let flexibleSpace = UIBarButtonItem(
            barButtonSystemItem: .flexibleSpace,
            target: nil,
            action: nil
        )
        self.toolbarItems = [flexibleSpace, shareButton]
        self.navigationController?.isToolbarHidden = true
    }
    
    func add(item: String) {
        self.shopList.insert(item, at: 0)
        self.tableView.reloadData()
        self.navigationController?.isToolbarHidden = false
        self.navigationItem.leftBarButtonItem?.isEnabled = true
    }
}

// MARK: - Objc Methods

@objc
private extension ShopListTableViewController {
    func showInputAlert() {
        let alertVC = UIAlertController(
            title: "Enter the item",
            message: "",
            preferredStyle: .alert
        )
        alertVC.addTextField()
        
        let submitAction = UIAlertAction(
            title: "Submit",
            style: .default
        ) { _ in
            guard let text = alertVC.textFields?.first?.text,
                  !text.isEmpty
            else {
                return
            }
            self.add(item: text)
        }
        
        let cancelAction = UIAlertAction(
            title: "Cancel",
            style: .destructive
        ) { _ in }
        
        alertVC.addAction(submitAction)
        alertVC.addAction(cancelAction)
        
        self.present(alertVC, animated: true)
    }
    
    func resetShopListData() {
        self.shopList = []
        self.tableView.reloadData()
        self.navigationController?.isToolbarHidden = true
        self.navigationItem.leftBarButtonItem?.isEnabled = false
    }
    
    func shareItems() {
        let items = self.shopList.joined(separator: "\n")
        let activityController = UIActivityViewController(
            activityItems: [items],
            applicationActivities: nil
        )
        activityController.excludedActivityTypes = [.airDrop, .assignToContact, .copyToPasteboard]
        self.present(activityController, animated: true)
    }
}
